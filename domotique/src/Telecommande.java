import java.util.*;

/**
 * classe qui represente une telecommande
 * @author paon
 */
public class Telecommande {
	
	/**
	 * liste de toutes les machines que gere la telecommande
	 */
	private List<Machine> machines;
	
	/**
	 * constructeur qui cree une telecommande avec initialement aucune machine
	 */
	public Telecommande() {
		this.machines=new ArrayList<Machine>();
	}
	
	/**
	 * permet d ajouter une machine a la telecommande
	 * @param m la machine a ajouter
	 */
	public void ajouter(Machine m) {
		this.machines.add(m);
	}
	
	/**
	 * permet d activer une machine a partir de son indice
	 * @param i la place de la machine dans la liste
	 */
	public void activer(int i) {
		this.machines.get(i).allumer();
	}
	
	/**
	 * permet d activer une machine a partir de son indice
	 * @param i la place de la machine dans la liste
	 */
	public void desactiver(int i) {
		this.machines.get(i).eteindre();
	}
	
	/**
	 * permet d afficher les elements de la telecommande
	 * @return la chaine de caractere presentant les machines presentent dans la telecommande
	 */
	public String toString() {
		String res="";
		for (int i=0 ; i<this.machines.size() ; i++) {
			res=res+"Machine n°"+(i+1)+" : "+this.machines.get(i).toString()+"\n";
		}
		return res;
	}
	
	/**
	 * permet d acceder a la liste des machines que possede une telecommande
	 * @return la liste des machines
	 */
	public List<Machine> getMachines() {
		return machines;
	}
	
	/**
	 * permet de connaitre la taille de la liste des machines de la telecommande
	 * @return la taille de la liste des machines
	 */
	public int getNombre() {
		return this.machines.size();
	}
	
}
