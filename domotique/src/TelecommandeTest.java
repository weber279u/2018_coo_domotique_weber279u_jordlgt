import static org.junit.Assert.*;
import org.junit.*;

public class TelecommandeTest {
	
	Telecommande t;
	Machine l1;
	Machine l2;
	Machine h3;
	Machine lum4;
	
	@Before public void initialisation() {
		t=new Telecommande();
		l1=new Lampe("lampe 1");
		l2=new Lampe("lampe 2");
		h3=new Hifi();
		lum4=new AdapterLumiere();
	}
	
	@Test
	/**
	 * test de la methode ajouter sur une telecommande vide
	 */
	public void test_01_ajoutTelecommandeVide() {
		// methode testee
		t.ajouter(l1);
		t.ajouter(h3);
		t.ajouter(lum4);
		
		// verification
		assertEquals("La lampe en tete ne devrait pas etre null", l1, t.getMachines().get(0));
		assertEquals("La chaine Hi-fi ne devrait pas etre null", h3, t.getMachines().get(1));
		assertEquals("La lumiere ne devrait pas etre null", lum4, t.getMachines().get(2));
	}
	
	
	@Test
	/**
	 * test de la methode ajouter sur une telecommande possedant deja une machine
	 */
	public void test_02_ajoutTelecommande1Element() {
		// methode testee
		t.ajouter(l1);
		t.ajouter(l2);
		
		// verification
		assertEquals("La lampe en tete ne devrait pas etre null", l1, t.getMachines().get(0));
		assertEquals("La lampe en 2eme position ne devrait pas etre null", l2, t.getMachines().get(1));
	}
	
	
	@Test
	/**
	 * test de la methode activer sur une machine existante en position 0
	 */
	public void test_03_activerMachine() {
		// methode testee
		t.ajouter(l1);
		t.ajouter(h3);
		t.ajouter(lum4);
		t.activer(0);
		t.activer(1);
		t.activer(2);
		
		// verification
		assertEquals("La lampe devrait etre allumee", true, ((Lampe)t.getMachines().get(0)).isAllume());
		assertEquals("La chaine Hi-fi devrait etre allumee", 10, ((Hifi)t.getMachines().get(1)).getSon());
		assertEquals("La lumiere devrait etre allumee", 100, ((AdapterLumiere)t.getMachines().get(2)).getLumiere().getLumiere());
	}
	
	
	@Test
	/**
	 * test de la methode activerLampe sur une lampe inexistante
	 */
	public void test_04_activerMachineInexistante() {
		try {
			t.activer(0);
		} catch(Exception e) {
			System.out.println("Machine inexistante");
		}
	}
}
