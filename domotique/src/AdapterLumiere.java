
public class AdapterLumiere implements Machine{
	
/**
 * lumiere manipulable	
 */
	private Lumiere lumiere;
	
/**
 * constructeur par defaut
 */
	public AdapterLumiere() {
		this.lumiere=new Lumiere();
	}

/**
 * permet d allumer une lumiere donc de mettre son intensite a 100
 */
	public void allumer() {
		this.lumiere.changerIntensite(100);
	}

/**
 * permet d eteindre une lumiere donc de mettre son intensite a 0
 */
	public void eteindre() {
		this.lumiere.changerIntensite(0);
	}

/**
 * getteur
 * @return la lumiere
 */
	public Lumiere getLumiere() {
		return lumiere;
	}
	
	public String toString() {
		return this.lumiere.toString();
	}
}
